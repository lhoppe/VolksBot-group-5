﻿using System;
using BotatHWR.Bot;
using static BotatHWR.Bot.Engine;

namespace BotatHWR
{
    class Program
    {
        static Arm arm = new Arm();
        static Engine engine = new Engine(EngineType.ROBOT);

        static void Main(string[] args)
        {
            Console.WriteLine("up - arm moves up");
            Console.WriteLine("down - arm moves down");
            Console.WriteLine("join - hand closes");
            Console.WriteLine("divide - hand opens");
            Console.WriteLine("exit - finishes the program");
            Console.WriteLine("forward - wheel moves forward");
            Console.WriteLine("backwards - wheel moves backwards");
            Console.WriteLine("leftcurve - right wheel moves forwards, left wheel moves backwards");
            Console.WriteLine("rightcurve - left wheel moves forwards, right wheel moves backwards");
            Console.WriteLine("course - follows example course");


            // Variable consoleInput wird angelegt mit Format String
            string consoleInput = "";
            // SO lange nicht exit gedrückt wird
            while (consoleInput != "exit")
            {
                // Definiere, dass bei der Eingabe von consolInput die Zeile gelesen werden soll
                consoleInput = Console.ReadLine();
                //Switch wird verwendet für Fallunterscheidung (Ersatz für viele ifs); überprüft, welcher Befehl hinter switch eingegeben wird
                switch (consoleInput)
                {
                    //Zugriff auf das, was im Ordner Bot > Arm.cs vordefiniert ist
                    case "up":
                        arm.up();
                        break;
                    case "down":
                        arm.down();
                        break;
                    case "join":
                        arm.join();
                        break;
                    case "divide":
                        arm.divide();
                        break;
                    case "forward":
                        engine.MoveToPosition1(66);
                        engine.MoveToPosition2(66);
                        break;
                    case "backwards":
                        engine.MoveToPosition1(-66);
                        engine.MoveToPosition2(-66);
                        break;
                    case "leftcurve":
                        engine.MoveToPosition1(-66);
                        engine.MoveToPosition2(66);
                        break;
                    case "rightcurve":
                        engine.MoveToPosition1(66);
                        engine.MoveToPosition2(-66);
                        break;
                    case "course":
                        Course();
                        break;

                }
            }
        }
        static void Course()
        {

            engine.MoveToPosition1(-66);
            engine.MoveToPosition2(66);
            while (!engine.MovementState1 || !engine.MovementState2)
            {
                // wait for 1000 ms
                System.Threading.Thread.Sleep(1000);
            }
           
            engine.MoveToPosition1(100);
            engine.MoveToPosition2(100);
            
        }
    }
}